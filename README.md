# Present List

List of present ideas if you want to give Mosel a present.


## For conservatorio:

- New cello

- Cuerdas cello/cello strings - [link to cello strings](https://luthiervidal.com/en/violoncellos-and-accessories/cellos-strings/cello-string-larsen/cello-string-larsen-set.html#/5-medida-4_4)


## For cycling:

- Ropa de ciclista/cycling clothes 

- Botas de ciclista para bici de montaña /cycling shoes for mountain bike - [something like this](https://www.scott-sports.com/us/en/product/scott-mtb-comp-boa-lady-shoe?article=2518385552016)

Size 40


## Games:

- R4 card 3DS – [link to shop](https://www.r43ds.org/products/R4-3DS.html)

Note: I’m not sure if this one comes with micro SD card 


## Other

- Satchel/bag for computer + books + kindle etc

- Noise cancelling headphones [link to sound isolatig/cuality studio headphnes](https://www.audio-technica.com/en-us/ath-m50x)

Note: [You may find a better price here](https://www.idealo.es/resultados.html?q=ATH-M50x+)

- Silla de oficina/comfortable office chair 


